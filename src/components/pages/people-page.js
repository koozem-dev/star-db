import React from 'react';
import { PersonDetails, PersonList } from '../sw-component';
import Row from '../row';
import { useNavigate, useParams } from 'react-router-dom';

const PeoplePage = () => {

  const navigate = useNavigate();
  const { id } = useParams();

  return (
    <Row
      left={<PersonList onItemSelected={(itemId) => navigate(itemId)} />}
      right={<PersonDetails itemId={id} />} />
  );
};

export default PeoplePage;