import React from 'react';
import { StarshipList } from '../sw-component';
import { useNavigate } from 'react-router-dom';

const StarshipPage = () => {
  const navigate = useNavigate();

  return (
    <StarshipList
      onItemSelected={(itemId) => navigate(itemId)} />
  );
};

export default StarshipPage;