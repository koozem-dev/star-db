import React, { Component } from 'react';

import ErrorButton from '../error-button/error-button';
import Spinner from '../spinner';

import './item-details.css';

const Record = ({ item, field, label }) => {
  return (
    <li className="list-group-item">
      <span className="term">{label}</span>
      <span>{item[field]}</span>
    </li>
  );
};

export {
  Record,
};

export default class ItemDetails extends Component {

  state = {
    item: {},
    //loading: true,
    image: null,
  };

  componentDidMount() {
    this.updateItem();
  }

  componentDidUpdate(prevProps, prevState, snapshot) {
    if (this.props.itemId !== prevProps.itemId ||
      this.props.getData !== prevProps.getData ||
      this.props.getImageUrl !== prevProps.getImageUrl) {
      this.updateItem();
    }
  }

  updateItem() {
    const { itemId, getData, getImageUrl } = this.props;
    if (!itemId) {
      return;
    }

    getData(itemId).then((item) => {
      this.setState({
        item,
        image: getImageUrl(item),
      });
    });
  }

  render() {

    const { item, loading, image } = this.state;

    if (!item) {
      return <span>Selected a item from the list</span>;
    }

    const content = !loading ?
      <ItemsView
        item={item}
        image={image}
        children={this.props.children} /> : null;
    const loader = loading ? <Spinner /> : null;

    return (
      <div className="item-details card">
        {loader}
        {content}
      </div>
    );
  }
}

const ItemsView = ({ item, image, children }) => {

  const { name } = item;

  return (
    <React.Fragment>
      <img className="item-image"
           src={image}
           alt="item" />

      <div className="card-body">
        <h4>{name}</h4>
        <ul className="list-group list-group-flush">
          {
            React.Children.map(children, (child) => {
              return React.cloneElement(child, { item });
            })
          }
        </ul>
        <ErrorButton />
      </div>
    </React.Fragment>
  );
};