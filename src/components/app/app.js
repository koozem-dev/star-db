import React, { Component } from 'react';
import ErrorBoundry from '../error-boundry';
import SwapiService from '../../services/swapi-service';
import DummySwapiService from '../../services/dummy-swapi-swrvice';

import {
  LoginPage,
  PeoplePage,
  PlanetPage,
  SecretPage,
  StarshipPage,
} from '../pages';
import { SwapiServiceProvider } from '../swapi-service-context';

import './app.css';
import { BrowserRouter, Route, Routes } from 'react-router-dom';
import Header from '../header';
import { StarshipDetails } from '../sw-component';
import RandomPlanet from '../random-planet';

export default class App extends Component {

  state = {
    swapiService: new SwapiService(),
    isLoggedIn: false,
  };

  onLogin = () => {
    this.setState({
      isLoggedIn: true,
    });
  };

  onServiceChange = () => {
    this.setState(({ swapiService }) => {

      const Service = swapiService instanceof SwapiService ?
        DummySwapiService :
        SwapiService;
      console.log('change', Service.name);
      return {
        swapiService: new Service(),
      };
    });
  };

  render() {

    const { isLoggedIn } = this.state;

    return (
      <ErrorBoundry>
        <SwapiServiceProvider value={this.state.swapiService}>
          <BrowserRouter>
            <div className="stardb-app">
              <Header onServiceChange={this.onServiceChange} />
              <RandomPlanet />
              <Routes>
                <Route path="/" element={<h2>Welcome to StarDB</h2>} />
                <Route path="/people/" element={<PeoplePage />}>
                  <Route path=":id" element={<PeoplePage />} />
                </Route>
                <Route path="/planets/" element={<PlanetPage />} />
                <Route path="/starships/" element={<StarshipPage />} />
                <Route path="/starships/:id" element={<StarshipDetails />} />
                <Route path="/login" element={
                  <LoginPage
                    isLoggedIn={isLoggedIn}
                    onLogin={this.onLogin}
                  />} />
                <Route path="/secret"
                       element={<SecretPage isLoggedIn={isLoggedIn} />} />
                <Route path="*" element={<h2>Oops! Page not found!</h2>} />
              </Routes>
            </div>
          </BrowserRouter>
        </SwapiServiceProvider>
      </ErrorBoundry>
    );
  }
}
